<?php
    session_start();

    if (!isset($_SESSION['login'])){
        header("location:login.php");
    }

    if (isset($_SESSION['DELETE'])){
        echo $_SESSION['DELETE'];
        unset ($_SESSION['DELETE']);
    }

    if (isset($_SESSION['UPDATE'])){
        echo $_SESSION['UPDATE'];
        unset ($_SESSION['UPDATE']);
    }



    require_once('database.php');

    $select_query = "SELECT * FROM contact";
    $information = mysqli_query($connection,$select_query);

?>



<h1> ALL CONTACT </h1>
<form class="" action="logout.php" method="post">
    <button  class="btn btn-danger" type="submit" name = "logout" value = "one">Logout</button>
</form>

<table class="table table-striped table-dark table-bordered">
    <thead>
    <tr>
        <th scope="col">ID</th>
        <th scope="col">Full Name</th>
        <th scope="col">Email</th>
        <th scope="col">Subject</th>
        <th scope="col">Message</th>
        <th scope="col">Action</th>
    </tr>
    </thead>

    <tbody>

    <?php
        $no = 1;
        foreach($information as $single_data) { ?>
    <tr>
        <td><?= $no  ?> </td>
        <td><?= $single_data['name']  ?></td>
        <td><?= $single_data['email']  ?></td>
        <td><?= $single_data['subject']  ?></td>
        <td><?= $single_data['message']  ?></td>
        <td> <a href="delete.php?contact_id= <?= $single_data['id'] ?>" > Delete </a> |
             <a href="edit.php?contact_id= <?= $single_data['id'] ?>" > Edit </a> </td>
    </tr>
    <?php
        $no++;
        }
    ?>
    </tbody>
</table>
